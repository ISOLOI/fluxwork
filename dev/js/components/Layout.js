import React from "react";
import Nouislider from 'react-nouislider';
import Reflux from 'reflux';
import Actions from '../reflux/HandleActions';
import HandleStore from '../reflux/HandleStore';
import SliderButtonsComponent from './SliderButtonsComponent';
require('../../scss/style.scss');

var Layout = React.createClass({

    getInitialState: function () {
      console.log(HandleStore.getInitialState());
        return HandleStore.getInitialState();

    },


    onClick: function () {

    },
    componentDidMount: function() {
         this.unsubscribe = HandleStore.listen(this._onChange);
     },
     componentWillUnmount: function() {
         this.unsubscribe();
     },
    _onChange: function (state) {
        this.setState(state);
    },

    render() {
      let sliderButtons = [];
        for (var i = 0; i < this.state.handles.length; i++) {
            console.log(this.state.sizes[i].title);
            let label = this.state.sizes[i].title;
            sliderButtons.push(<SliderButtonsComponent  key={i} label={this.state.label}/>);
        }


        return (
            <div>
                <div className="container">
                    <div className="row">
                        <br /><br />
                        <div className="row">
                            <div className="col-md-12 col-sm-12">
                                <div className="slider" id="demoSlider">
                                    <Nouislider
                                        pips={{
                                            mode: 'range',
                                            density: 5
                                        }}
                                      range={this.state.range}

                                        start={this.state.handles}
                                        />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="pull-right">
                                <i className="fa fa-plus-circle" onClick={Actions.ButtonClick}></i>
                            </div>
                            <div className="button-wrapper" id="buttons">
                                {sliderButtons}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

export default Layout;
