import React from 'react';
import Reflux from 'reflux';
import Actions from '../reflux/HandleActions';
import InputBox from './InputBox';




var SliderButtonsComponent = React.createClass({
    render: function () {

        return (
            <div>
                <div className="col-md-12">
                  <div className="small" id="extra" onChange={this.onChange}>
                  <InputBox />
                  <i className="fa fa-remove" onClick={Actions.MinusClick}></i>
                  </div>
                </div>
            </div>
        );
    }
});

export default SliderButtonsComponent;
