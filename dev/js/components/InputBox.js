import React from 'react';

var InputBox = React.createClass({
    getInitialState: function () {
        return {typed: ''};
    },
    onBlur: function (event) {
        this.setState({typed: event.target.value});
    },
    onChange: function() {
    },
    render: function () {
        return(
          <div>
            <input id="media-size"  type="text"  onBlur={this.onBlur}/>

        </div>
      );
    }
});

export default InputBox;
