import Reflux from 'reflux';
var Actions = Reflux.createActions([
  'ButtonClick',
  'MinusClick',
  'CheckClick'
]);

export default Actions
