import Actions from './HandleActions';
import Reflux from 'reflux';

var HandleStore = Reflux.createStore({
    listenables: Actions,
    getInitialState: function () {

        this.state = {
            "sizes": [
                {
                    "title": "Mobile",
                    "min": 0,
                    "max": 100

                },
                {

                    "title": "Tablet",
                    "min": 800,
                    "max": 1000

                },
                {
                    "title": "Desktop",
                    "min": 1000,
                    "max": null
                }
            ],
            "handles": [
                459, 999, 1300
            ],
            "range": {
              "min": 0,
              "25%": 460,
              "50%": 1100,
              "max": 1600
            },
        }
        return this.state;
    },


    onButtonClick: function() {
    this.state.handles.push(this.state.handles[this.state.handles.length -1] + 300);
    this.state.sizes[this.state.sizes.length -1].max=this.state.handles[this.state.handles.length -1];
    this.state.sizes.push({
    title: 'New Size',
        "min": this.state.handles[this.state.handles.length + 1],
        "max": null
    });
    this.trigger(this.state);
  },
  onMinusClick: function() {
    var el = document.getElementsByClassName('small');
        el[el.length-1].remove();
        //Remove Handle
        let newarray = this.state.handles.splice(0,1);
        this.trigger(this.state);
        console.log(this.state.handles);
  },

    fireUpdate: function () {

    }
});

export default HandleStore;
